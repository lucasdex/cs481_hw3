import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Color _color = Colors.white;

  GestureDetector createContainer(Color color, String text) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _color = color;
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: color,
            border: Border.all(color: Colors.blue, width: 5.0),
            borderRadius: BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(30),
        child: Text(text),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Here is my app !", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text("You can use these buttons to change the background color of the app :",
                style: TextStyle(fontWeight: FontWeight.w500),),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  createContainer(Colors.orange, "Orange"),
                  createContainer(Colors.green, "Green"),
                  createContainer(Colors.yellow, "Yellow"),
                ],
              ),
            ),
            createContainer(Colors.white, "White")
          ],
        ),
      ),
      backgroundColor: _color,
    );
  }
}
